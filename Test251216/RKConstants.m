//
//  RKConstants.m
//  Test251216
//
//  Created by Roman Kazmirchuk on 25.12.16.
//  Copyright © 2016 Roman Kazmirchuk. All rights reserved.
//

#import "RKConstants.h"

@implementation RKConstants

#pragma mark - Color

+ (UIColor *)fileCellOrangeColor {

    return [UIColor colorWithRed:240/255.
                           green:104/255.
                            blue:48/255.
                           alpha:1];

}

+ (UIColor *)fileCellBlueColor {

    return [UIColor colorWithRed:30/255.
                           green:179/255.
                            blue:231/255.
                           alpha:1];

}

@end
