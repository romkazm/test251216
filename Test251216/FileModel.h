//
//  FileModel.h
//  Test251216
//
//  Created by Roman Kazmirchuk on 25.12.16.
//  Copyright © 2016 Roman Kazmirchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FileType) {
    
    FileTypeImage = 1,
    FileTypePdf,
    FileTypeMovie,
    FileTypeMusic,
    FileTypeOther
    
};

@interface FileModel : NSObject

@property (strong, nonatomic) NSString *filename;
@property (assign, nonatomic) BOOL isFolder;
@property (strong, nonatomic) NSDate *modDate;
@property (assign, nonatomic) FileType fileType;
@property (assign, nonatomic) BOOL isOrange;
@property (assign, nonatomic) BOOL isBlue;

@end
