//
//  RKConstants.h
//  Test251216
//
//  Created by Roman Kazmirchuk on 25.12.16.
//  Copyright © 2016 Roman Kazmirchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RKConstants : NSObject

+ (UIColor *)fileCellOrangeColor;
+ (UIColor *)fileCellBlueColor;

@end
